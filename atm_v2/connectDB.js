import fs from 'fs';

const print = console.log;

export const readFromDatabase = (filePath, callback) => {
    let toReturn;
    fs.readFile(filePath, "utf8", (err, filePath) => {
        callback = (callback) ? callback : 0;
        if (err) print(err)
        else callback(JSON.parse(filePath));
           
    })
} 

export const writeToDatabase = (filePath, dataToSave) => {
    const data = JSON.stringify(dataToSave, null, 4);
    fs.writeFile(filePath, data, "utf8", (err) => {
        if (err) print("Saving failed.")
    })
}


//export default readFromDatabase;