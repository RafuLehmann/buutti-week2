import { CronJob } from "cron";
import fs from 'fs';
import { readFromDatabase as readDB, writeToDatabase as writeDB } from "./connectDB.js";

const dailyLog = new CronJob('7 23 * * *', function() {
    readDB("log.json", (content) => {
    const newContent = content;
    const today = new Date(Date.now())
                        .toISOString()
                        .replace(/(:\d{1,}.\d{1,}Z)$/,"")
                        .replace("T", "_")
                        .replace(/(\:|\-)/g, "")

    const dailyDir = './dailyLogs/'
    const dailyFile = `${dailyDir}log_${today}.json`

    console.log(dailyFile);
    if (fs.existsSync(dailyDir)) {
        console.log('Directory exists!');
    } else {
        console.log('Directory not found, creating one now.');
        fs.mkdir('./dailyLogs/', {recursive: false}, (err) => {
            if (err) console.log(err);
        })
    }
    writeDB(dailyFile, newContent);
    writeDB("log.json", []);
    })
    console.log("running cronjon every day @ 22.15.")
    
}, null, true, 'Europe/Helsinki')
dailyLog.start()

