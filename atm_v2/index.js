import * as readline from 'node:readline';
import { stdin as input, stdout as output } from 'process';
import crypto from 'crypto';
import { readFromDatabase, writeToDatabase } from './connectDB.js';



let print = console.log;
const userID = process.argv[2] | "unkown";

let balance = 1000;
const mainQuestion = 
`\nWhat would you like to do next?
1. Check your balance
2. Do a withdrawal
3. Do a contribution
Q. Quit
Your response: `;


/**
 * Log all events 
 */
const logAction = (action, total, amount = 0) => {
    const file = "log.json";
    const logDetails = {
        uuid: crypto.randomUUID(),
        event: action,
        timestamp: Date.now(),
        "user-id": userID,
        spent: amount,
        balance : total
    };
    readFromDatabase(file, (content) => {
        const newContent = content;
        newContent.push(logDetails);
        writeToDatabase(file, newContent);
    } )
}

/* const checkBalance = () => {
    cosnt previousLog
    readDB("log.json", (content) => {
        const filter_user = content.filter(user => user["user-id"] === 123)
        previousLog = filter_user[filter_user.length-1];
    }) 
    print(previousLog.balance)
} */
/** Function that displays the amount on the account **/
const showBalance = () => {
    print(`Your balance is ${balance}.`);
    logAction("Balance", balance);
    main();
}


/** 
 * Function that allows customer to make a contribution 
 * or to make a withdrawal.
 **/
const contributeOrWithdraw = async (action) => {
    let request = 0;
    if(/^[^2-3]$/.test(action)) return 0;
    let response = await askQuestion(
        `How much would you like to ${action}?\nYour response: `)
		.then(amount => request = Number(amount))
		.then(amount => balance = balance-amount)
		.then(result => {print(`You ${action}ed ${request} and have ${result} left on your account.`);logAction(action, result, request)})
        .finally(() => main());
}
    
    
    /** 
     * Function that calls function depending on users 
     * selection.
     **/
const selectedAction = (answer) => {
    switch(answer) {
        case "1":
            showBalance();
            break;
        case "2":
            contributeOrWithdraw("withdraw");
            break;
        case "3":
            contributeOrWithdraw("contribute");
            break;
        case "q":
        case "Q":
            print("Thank you!");
            logAction("logout", balance);
            return 'q';
        default:
            print("invalid input");
            logAction("input failure", 0);
            return 'q';
            
        }
}

/**** 
 * Function that asks a question and returns the response 
 * from stdin
 **/
const askQuestion = (question) => {
    const rl = readline.createInterface({ input, output});
    
    return new Promise(resolve => rl.question(question, answer => {
        resolve(answer);
        rl.close();
    }));
}


const main = async () => {
    let ans = "";
    ans = await askQuestion(mainQuestion)
    .then(answer => selectedAction(answer));
}

/**** If there is a login attempt without id a warning will be displayed and attempt will be logged.****/
if (process.argv.length !== 3) {
    logAction("suspicious_login", balance);
    print(`Login attempt failed, add your user-id. \n'--> node index.js <user-id>'`);
} else {
    logAction("login", balance);
    main();
}