/** 
 * Reverse words in sentence
 * 
 * Create a programs that reverses each word in a string.
 *  
 * Example
 *  node .\reverseWords.js "this is a very long sentence" 
 *  -> sihT si a yrev gnol ecnetnes
 */

const input = process.argv.slice(2);
const output = input.map(sentence => sentence
                        .split(" ")
                        .map(word => word
                            .split("")
                            .reverse()
                            .join("")
                        ).join(" ")
                    ).join(" ");
console.log(output)