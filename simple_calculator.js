/** 
 * CALCULATOR
 */

const input = process.argv.slice(2);

/** 
 * Starts with validation of input. If inputs passes 
 * the validation, calculation starts. Tackled the issue 
 * with inputting following symbols in teh terminal / , //.
 * TODO: fix issue with * which lists all files instead 
 * of working as a multiplier.  
 **/
const simpleCalculator = (array) => {

    const isNum = /^[\x30-\x39]+$/;
    const isOp = /^(\x5E|\x25|\x78|\x58|\x2B|\x2D|[\x2F]{1-2})$/;
    const isDiv = /[\x2F]/;
    switch (true) {
        case array.length !== 3:
            const rules = "\nrules: \n+ sum\t\t- diff\nx multiply\t/ divide\n% modulo\t^ power \n// nth rooth";
            console.log("not enough arguments, add your calculation like this: 1 + 1"+rules);
            return false;
        case !isNum.test(array[0]):
            console.log(array[0], "is not a number");
            return false;
        case !isNum.test(array[2]):
            console.log(array[2], "is not a number");
            return false;
        case isDiv.test(array[1]):
            if(array[1].match(/\//g).length === 2) calc("/", Number(array[0]), Number(array[2]));
            else calc("//", Number(array[0]), Number(array[2]));
            return false;
            case !isOp.test(array[1]):
                console.log(array[1], "is not a operator");
                return false;
                default:
                    calc(array[1], Number(array[0]), Number(array[2]));
            return true;
    }
}

/**
 * selecting right operation based on given symbol.
 */
const calc = (operator, num1, num2) => {
    let result = 0;
    if (operator.length > 3) operator = '/';
    switch (operator){
        case '+':
            result = num1 + num2;
            console.log(num1, operator, num2, "=", result);
            return result;  
        case '-':
            result = num1 - num2;
            console.log(num1, operator, num2, "=", result);
            return result;  
        case 'x':
            result = num1 * num2;
            operator = '*';
            console.log(num1, operator, num2, "=", result);
            return result;    
        case '/':
            result = num1 / num2;
            console.log(num1, operator, num2, "=", result);
            return result;  
        case '^':
            result = Math.pow(num1,num2);
            console.log(num1, operator, num2, "=", result);
            return result;  
        case '//':
            result = Math.round(Math.pow(num1,1/num2)* 100) / 100;
            console.log(num1, operator, num2, "=", result);
            return result;  
        case '%':
            result = num1 % num2;
            console.log(num1, operator, num2, "=", result);
            return result;  
        default:
            result = "Invalid input! use the following operators: \n+ sum\t\t- diff\n* multiply\t/ divide\n% modulo\t^ power \n// nth rooth";
            console.log(result);
            return result;
    }
};

simpleCalculator(input);

