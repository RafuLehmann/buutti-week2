/**
 * Randomize order of array
 * 
 * Create a program that every time you run it, prints 
 * out an array with differently randomized order of 
 * the array above.
 * 
 * Example:
 *  const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];
 *  
 *  node .\randomizeArray.js 
 *  outputs -> [5, 4, 18, 32, 8, 6, 2, 25, 14, 10]
 */

/* the original array */
const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];

/* the randomised array */
const randArr = []; 


/** 
 * Randomiser function that returns 
 * a new index to the element 
 **/

const newIndexNo = (arrayLength) => {
    const max = arrayLength;
    return Math.floor(Math.random() * max);
}


/** 
 * iterates through the original 
 * array and copies the elements to 
 * random positions in the new array  
 * */

array.map(num => randArr.splice(newIndexNo(array.length), 0, num))
console.log(randArr)