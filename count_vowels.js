
// a function that returns the sum of vowels
const getVowelCount = (str) => {
    let vowels = /[a, e, i, o, u, y]/gi;
    return str.match(vowels).length;
}


getVowelCount('abraceadabra');