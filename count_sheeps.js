/**
 * Create a program that takes in a number 
 * from the command line, for example 
 * node .\countSheep.js 3
 * 
 * and prints a string 
 * "1 sheep...2 sheep...3 sheep..."
 */

const input = process.argv[2];


const countSheep = (n) => {
    let count = 1
    while (0 < n) {
        process.stdout.write(`${count} sheep...`);
        count++;
        n--;
    }
}
countSheep(input);