/**
 * const charIndex = {  a : 1, b : 2, c : 3, d : 4, 
 *                      e : 5, ... , y : 25, z : 26 };
 * 
 * Create a program that turns any given 
 * word into charIndex version of the word
 * 
 * Example:
 *  node .\charIndex.js "bead" -> 2514
 *  node .\charIndex.js "rose" -> 1815195 
 **/

// moving the input to a new array
const input = process.argv.slice(2);

// splitting words into own elements;
const splittedArr = input.map(word => word.split(""));

// table used for converting
const charIndex = { a : 1, b : 2, c : 3, d : 4, 
                    e : 5, f : 6, g : 7, h : 8, 
                    i : 9, j: 10, k: 11, l: 12,
                    m: 13, n: 14, o: 15, p: 16,
                    q: 17, r: 18, s: 19, t: 20,
                    u: 21, v: 22, w: 23, x: 24,
                    y: 25, z: 26 };

// converting numbers to chars
const result = splittedArr.map(word => 
                word.map(
                    char => charIndex[char])
                    .join("")
                    ).join(" ");

// output result
console.log(result)