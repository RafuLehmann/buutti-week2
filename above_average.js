
const arr0 = [1, 5, 9, 3]
const arr1 = [1, 4, 23]
const arr2 = [1, 5, 9, 3, 6, 3]
const arr3 = [1, 5, 9, 11, 3, 34]
const arr4 = [2, 2, 3, 2]


const aboveAverage = (arr) => {
    const len = arr.length;
    const sum = arr.reduce((tot, val) => tot + val)
    const avg = sum/len;
    console.log(avg, ":", arr, "=>",arr.filter(elem => elem >= avg))
    return arr.filter(elem => elem >= avg);
}

aboveAverage(arr0);
aboveAverage(arr1);
aboveAverage(arr2);
aboveAverage(arr3);
aboveAverage(arr4);