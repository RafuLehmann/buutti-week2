/**
 * Ordinal numbers
 * 
 * You have two arrays:
 * const competitors = ["Julia", "Mark", "Spencer", 
 *                      "Ann" , "John", "Joe"]; 
 * 
 * const ordinals = ['st', 'nd', 'rd', 'th'];
 * 
 * Create program that outputs competitors placements 
 * with following way: 
 * ['1st competitor was Julia', '2nd competitor was Mark', 
 * '3rd competitor was Spencer', '4th competitor was Ann', 
 * '5th competitor was John', '6th competitor was Joe']
 * 
 */


const competitors = ["Julia", "Mark", "Spencer", 
                      "Ann" , "John", "Joe"];

const placements = (players) => {
    const ordinals = ['st', 'nd', 'rd', 'th'];

    // array that will be returned
    let placement = [];

    /** Iterating through all competitors and 
     * assigning the placement of each player. */
    players.map((player, index, arr) => {
        if (arr.indexOf(player) >= 3) {
            placement.push(`${index+1}${ordinals[3]} competitor was ${player}.`);
        } else {
            placement.push(`${index+1}${ordinals[index]} competitor was ${player}.`);
        }
    })
    return placement;
}
console.log(placements(competitors));

