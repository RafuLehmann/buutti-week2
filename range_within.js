/** 
 * Range within
 * 
 * Write a program that takes in any two numbers from the 
 * command line, start and end. The program creates and 
 * prints an array filled with numbers from start to end.
 * 
 * Examples:
 *  node .\createRange.js 1 5 -> [1, 2, 3, 4, 5]
 *  node .\createRange.js -5 -1 -> [-5, -4, -3, -2, -1]
 *  node .\createRange.js 9 5 -> [9, 8, 7, 6, 5]
 * 
 * Note the order of the values. When start is smaller than 
 * end, the order is ascending and when start is greater 
 * than end, order is descending 
 **/

const input = process.argv.slice(2).sort();

// validate input
if (input.length > 2 || input.length < 2) {
    const result = (input.length > 2) ? 
                    console.log("too many arguments") :
                    console.log("not enough arguments");
    process.exit();
}

/* Set MIN and MAX values */
let [min, max] = input;

/*Get the numbers between given range */
const rangeWithin = (max, min) => {
    let range = [];
    while (min <= max) {
        range.push(Number(min));
        min++;
    }
    return (range);
}
console.log(rangeWithin(max, min));