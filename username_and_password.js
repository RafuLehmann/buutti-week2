/**
 * USERNAME AND PASSWORD
 * 
 * Create a function (or multiple functions) that 
 * generates username and password from given firstname
 * and lastname.
 * 
 *  Username: 
 *  B + last 2 numbers from current year + 2 first letters 
 *  from both last name and first name in lower case
 * 
 *  Password: 
 *  1 random letter + first letter of first name in lowercase 
 *  + last letter of last name in uppercase + random special 
 *  character + last 2 numbers from current year
 * 
 *  Example: 
 *  John Doe -> B22dojo, mjE(20
 */

/**
 * Creates either random special characters 
 * or lowercase characters. To get special 
 * character the word "special" needs to be 
 * added as parameter.
 */ 
const randomAscii = (type = "") => {
    const max = 90;
    const min = 33;
    let ascii = Math.floor(Math.random()*(max-min+1)+min);
    
    if (type.toLowerCase() === "special") {
        while (!(ascii >= 33 && ascii <= 47)) {
            ascii = Math.floor(Math.random()*(max-min+1)+min);
        }
    }
    else {
        while (!(ascii >= 65 && ascii <= 90)) {
            ascii = Math.floor(Math.random()*(max-min+1)+min);
        }
    }
    return String.fromCharCode(ascii);
}


/** 
 *  Generates username based on:
 *  - letter 'B', 
 *  - year in format YY, 
 *  - first 2 character from last name in lowercase.
 *  - first 2 character from first name in lowercase.
 **/

const generateUserName = (fname, lname) => {
    const firstLatters = lname.slice(0,2)+ fname.slice(0,2);
    const yearYY = new Date().getFullYear().toString().slice(-2);
    return "B"+yearYY+firstLatters.toLowerCase();
}


/** 
 *  Generates password based on:
 *  - a random uppercase character, 
 *  - first character from firstname in lowercase,
 *  - last character from firstname in uppercase,
 *  - a random special character, 
 *  - year in format YY.
 **/

const generatePassWord = (fname, lname) => {
    const yearYY = new Date().getFullYear().toString().slice(-2);
    const firstLatters = fname.slice(0,1).toLowerCase();
    const lastLatters = lname.slice(fname.length -2).toUpperCase();
    return randomAscii()+firstLatters+lastLatters+randomAscii("special")+yearYY;
} 


const generateCredentials = (fname, lname) => {

    return [generateUserName(fname, lname), 
            generatePassWord(fname, lname)];
}
 
 
console.log(generateCredentials("John", "Doe").join(", "))