const input = process.argv[2];


 /*** INPUT VALIDATOR ***/
 if(!(/^[\x30-\x39]+$/.test(input))) {
     console.log("invalid input, ", input, ",only a number is accepted.")
     return(1);
    }
    
/*** MAKES CALCULATION BASED ON ODD OR EVEN NUMBERS UNTIL IT REACHES NO.1***/
const oddOrEven = (n, count = 0) => {
    if (n > 1){
        if (n % 2 === 0) {
            console.log(`${n} is even => ${n} / 2 =`,  n / 2)
            oddOrEven((n / 2), count+1)
        }
        else {
            console.log(`${n} is odd => ${n} * 3 + 1 =`, n * 3 + 1)
            oddOrEven((n * 3 + 1), count+1)
        }
    }
    else if (n === 1) 
        console.log(`And finally we reached nr. 1 after ${count} step(s).`);
    else if (n < 1) console.log("we got an issue here, number is below 1");
}
oddOrEven(input)