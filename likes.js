

const write = (str) => {
    process.stdout.write(str);
}

const likes = (likers) => {
    const lastIndex = likers.length-1;

    if (likers.length === 0) write("no one likes this\n")
    else if(likers.length < 4){

        likers.map( (liker, index) => {
            if (index === 0 )  return write(liker);
            else if( lastIndex === index) write(" and "+liker);
            else return write(", "+liker);
        });
        write(" likes this\n");

    } else {

        likers.map( (liker, index) => {
            if (index === 0 )  return write(liker);
            else if(index < 2) return write(", "+liker);
        });
        write(" and "+(likers.length-2)+" others\n");

    }
}

likes([])
likes(["Alex"])
likes(["Alex", "Linda"])
likes(["Alex", "Linda", "Mark"])
likes(["Alex", "Linda", "Mark", "Max"])
likes(["Alex", "Linda", "Mark", "Max", "Teppo"])

likes([]); 
likes(["John"]) 
likes(["Mary", "Alex"]) 
likes(["John", "James", "Linda"]) 
likes(["Alex", "Linda", "Mark", "Max"]) 