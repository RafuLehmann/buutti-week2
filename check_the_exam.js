/**
 * CHECK THE EXAM
 * 
 * The first input array is the key to the correct 
 * answers to an exam, like ["a", "a", "b", "d"]. 
 * 
 * The second one contains a student's submitted 
 * answers.The two arrays are not empty and are the 
 * same length.
 * 
 * Example 
 *  ["a", "a", "b", "b"], ["a", "c", "b", "d") => 6 
 * 
 *  Results:
 *  +4 for each correct answer
 *  -1 for each incorrect answer, 
 *  +0 for each blank answer, [""].
 * 
 *  If the score is < 0, returns 0
 */




const checkExam = (expected, answered) => {
    let score = 0;
    answered.map( (answer, index) => {
        if (answer === expected[index]) {
            score += 4;
        } else if (answer !== "") {
            score -= 1;
        }
    })

    if(score < 0) return 0;
    else return score;
}


let s1 = checkExam(["a", "a", "b", "b"], ["a", "c", "b", "d"])// → 6 
let s2 = checkExam(["a", "a", "c", "b"], ["a", "a", "b", ""])// → 7 
let s3 = checkExam(["a", "a", "b", "c"], ["a", "a", "b", "c"])// → 16 
let s4 = checkExam(["b", "c", "b", "a"], ["", "a", "a", "c"])// → 0 

console.log("s1",s1)
console.log("s1",s2)
console.log("s1",s3)
console.log("s1",s4)