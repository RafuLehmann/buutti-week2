/** 
 * isPalindrome 
 * 
 * Check if given string is a palindrome.
 * 
 * Examples:
 * 
 *  node .\checkPalindrome.js saippuakivikauppias 
 *  outputs -> "Yes, 'saippuakivikauppias' is apalindrome"
 * 
 *  node .\checkPalindrome.js saippuakäpykauppias 
 *  outputs -> "No, 'saippuakäpykauppias' is not a palindrome" 
 * */


/* ignoring the other arguments */
const input = process.argv[2];

if (process.argv.length !== 3) {
    console.error("Invalid input, add no more/less than one argument")
    process.exit()
}

const isPalindrome = (word) => {
    const reverseWord = input.split("").reverse().join("");

    if (word === reverseWord) {
        console.log(`Yes, ${word} is a palindrome`)
    } else {
        console.log(`No, ${word} is not a palindrome`)
    }
}

isPalindrome(input);